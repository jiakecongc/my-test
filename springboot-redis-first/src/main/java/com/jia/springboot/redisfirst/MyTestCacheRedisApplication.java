package com.jia.springboot.redisfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyTestCacheRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestCacheRedisApplication.class, args);
    }
}
