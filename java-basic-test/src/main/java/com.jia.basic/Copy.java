package com.jia.basic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Copy
{

    /**
     * 遍历文件夹下的所有图片文件，并复制到指定文件夹下F:\PDFbook\148-Redis核心技术与实战
     */
    static String srcfile = "F:\\PDFbook\\Java业务开发常见错误100例";//源目录
    static String destFile = "F:\\test\\Java业务开发常见错误100例\\";//源目录
    static String filetype = ".pdf";//关键字
    static List<File> flist = new ArrayList();//存放遍历的文件
    public static void main(String[] args)
    {
        File file = new File(srcfile);
        File[] list =file.listFiles();//源文件夹
        List<File> flist1 = new ArrayList();
        for(int i=0;i<list.length;i++){
            flist1.add(list[i]);
        }
        searchDirectory(flist1);
        wFile(flist,filetype);
        System.out.println("复制完成");
    }

    public static void searchDirectory(List<File> list){
        List<File> dlist0 = new ArrayList();//一级目录
        List<File> dlist1 = new ArrayList();//一级目录下的子目录
        //遍历的文件夹,将文件和文件夹分类
        for(File file: list){
            if(file.isDirectory()){
                dlist0.add(file);
            }else{
                flist.add(file);//文件存放到文件list中
            }
        }
        /**
         * 遍历子文件夹
         * 递归调用该方法，把目录和文件分开
         * */
        if(dlist0.size()>0||!dlist0.isEmpty()){
            for(int i=0;i<dlist0.size();i++){
                File[] list0 = dlist0.get(i).listFiles();
                for(int j=0;j<list0.length;j++){
                    dlist1.add(list0[j]);
                }
            }
            searchDirectory(dlist1);
        }
        return ;
    }
    //读写文件
    public static void wFile(List<File> flist,String filetype){
        for(File file :flist){
            if(file.isFile()){
                //包含关键字
                if(file.toString().toLowerCase().contains(filetype.toLowerCase())){
                    //把文件写到指定 的文件夹中
                    try
                    {
                        FileInputStream fileInputStream = new FileInputStream(file);
                        FileOutputStream outstrem = new FileOutputStream(new File(destFile+file.getName()));
                        outstrem.getChannel().transferFrom(fileInputStream.getChannel(), 0, fileInputStream.available());
                        //指定要复制的路径
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }


}
