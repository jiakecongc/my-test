package com.jia.basic;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 主要方法：
 * initialValue:初始化ThreadLoacl中的泛型对应的默认值
 * get:得到这个线程对应的value，如果获取不到，那么内部就会调用initialValue方法
 * set:为这个线程设置值
 * remove:删除这个线程对应的值
 * 一般来说，线程进来之前先去初始化一个可以泛型的ThreadLocal对象，之后只要这个线程在remove之前去调用这个get方法，那么都可以拿到初始化或者set进去的值
 * ThreadLocal可以数据隔离的方式就是
 *      每个线程内部都为何了自己的threadlocals(其实就是一个ThreadLocalMap对象，通过这个对象的entry属性可以获取这个线程的所有threadloacl)
 *      每个线程创建的ThreadLoacl数据都是存在自己的这个变量的内部。
 * 说明：
 *      业务代码能new好多个ThreadLocal对象，各司其职。但是在一次请求里，也就是一个线程里，ThreadLocalMap是同一个，而不是多个，
 *      不管你new几次ThreadLocal，ThreadLocalMap在一个线程里就一个，因为再说一次，ThreadLocalMap的引用是在Thread里的，
 *      所以它里面的Entry数组存放的是一个线程里你new出来的多个ThreadLocal对象
 *
 *
 * 总结就是：Thread类中维护了ThreadLocalMap, 而ThreadLocalMap里面维护了Entry数值，而Entry中存储的是以ThreadLoacl为key,传入的值为value的键值对
 *
 *
 * 内存泄露问题：ThreadLocal在保存在ThreadLocalMap中的时候，是以自身为key,值为value保存在entry中的，正常来说entry中的key和value都为强引用才对，但是现在entry中的key是
 *             弱引用，因此当threadlocal没有外部的强引用的时候（我理解的话，是指这个线程。这里错了，不是指线程，而是某一个对象），那么这个key就会被GC垃圾回收了，但是value确没有被回收，
 *             那么这时候大量的value就会存活，因此就会出现内存泄露的问题（例如线程池中的线程是一直存在的，所以threadlocal设置的value是一直存在的）。
 *             解决方式：1、使用的时候，记得调用remove方法
 *                      2、一般将threadlocal设置为static修饰的一个属性，属于类变量，这样这个threadlocal就属于类变量，类会有一个强引用，因此不会被回收
 *             https://zhuanlan.zhihu.com/p/304240519
 *             为什么要设置为弱引用：entry的key被设置为弱引用是为了让程序自动的对访问不到的数据进行回收。例如一个对象A使用了一个threadlocalA变量，如果这个对象A被垃圾回收了，那么就不会再
 *                                引用这个threadlocalA变量了，但是ThreadLocalMap中的entry还引用这它，因此它就不会被垃圾回收。即存在这种引用：
 *                                Thread -> ThreadLocal.ThreadLocalMap -> Entry[] -> Enrty -> key（threadLocal对象）和value
 *                                那么如果是这个引用链，就需要等到线程结束了才会再被垃圾回收了
 */
public class ThreadLocalTest05 {

    public static String dateToStr(int millisSeconds) {
        Date date = new Date(millisSeconds);
        /**
         * 通常情况下，我们创建的成员变量都是线程不安全的。因为它可能被多个线程同时修改，因此该变量对于多个线程之间并不是独立的，是共享变量
         *
         * 可以看这里，似乎我们可以发现每个线程都是创建了自己的SimpleDateFormat对象，所以似乎是在自己线程内部创建和这里的使用也是一样，
         * 但是，如果我们的线程是调用了很多个方法，同时每个方法的内部都会new这么一个对象，那么就会导致每次都new出来一堆的这个对象，而使用这种方式
         * 在同一个线程内部，只会保持一个SimpleDateFormat对象。因为这个对象是被ThreadLocal修饰的，因此可以做到在自己的线程内共享，而对其他线程隔离的效果
         */
        SimpleDateFormat simpleDateFormat = ThreadSafeFormatter.dateFormatThreadLocal.get();
//        SimpleDateFormat simpleDateFormat2 = ThreadSafeFormatter.dateFormatThreadLocal2.get();
        return simpleDateFormat.format(date);
    }

    private static final ExecutorService executorService = Executors.newFixedThreadPool(100);

    public static void main(String[] args) {
        for (int i = 0; i < 1; i++) {
            int j = i;
            executorService.execute(() -> {
                String date = dateToStr(j * 1000);
                // 从结果中可以看出是线程安全的，时间没有重复的。
                System.out.println(date + "---" + j);
            });
        }
        executorService.shutdown();
    }
}

class ThreadSafeFormatter {
    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal = new ThreadLocal() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        }
    };

//    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal2 = new ThreadLocal() {
//        @Override
//        protected SimpleDateFormat initialValue() {
//            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        }
//    };

    // java8的写法，装逼神器
//    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal =
//            ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));
}
