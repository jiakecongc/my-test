package com.jia.generic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestForUnderStand {

    /**
     * 没有泛型，每种类型都需要自定义
     * @param a
     * @param b
     * @param <T>
     * @return
     */
    private static int add(int a, int b) {
        System.out.println(a + "+" + b + "=" + (a + b));
        return a + b;
    }
    private static float add(float a, float b) {
        System.out.println(a + "+" + b + "=" + (a + b));
        return a + b;
    }
    private static double add(double a, double b) {
        System.out.println(a + "+" + b + "=" + (a + b));
        return a + b;
    }
    public static void main(String[] args) {
        double add = add(1, 2);
        System.out.println(add);
    }


    /**
     * 代码复用
     * @param a
     * @param b
     * @param <T>
     * @return
     */
    private static <T extends Number> double add(T a, T b) {
        System.out.println(a + "+" + b + "=" + (a.doubleValue() + b.doubleValue()));
        return a.doubleValue() + b.doubleValue();
    }

    /**
     * 泛型中的类型在使用时指定，不需要强制类型转换（类型安全，编译器会检查类型）
     */
    public static void test() {
        // 不指定类型，则可以添加任何类型，但是取出来的时候就需要强制类型转换，容易出错
        List list = new ArrayList();
        list.add(4);
        list.add("sdtd");

        // 指定类型，不需要强制类型转换，编译器会检查类型，提供编译前的检查
        List<String> listString = new ArrayList<>();
        listString.add("date");
//        报错，不可以添加数值类型，无法转换
//        listString.add(1);

    }


    /**
     *
     * <T> T getObject(Class<T> c)
     * 第一个<T> 声明这个方法为泛型方法。 在定义一个泛型方法时，必须在返回值的前边添加<T>,声明这是一个泛型方法，持有一个泛型T，然后才可以用作泛型T作为返回值
     * 第二个 T 指明该方法的返回值为类型T
     * 第三个 Class<T> 指明泛型T的具体类型，具体类型是调用方传进来的
     * 第四个 c 用来创建泛型T代表的类的对象
     *
     * @param c
     * @param <T>
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public <T> T getObject(Class<T> c) throws InstantiationException, IllegalAccessException {
        T t = c.newInstance();
        return t;
    }

    /**
     * 泛型的上下边界说明
     *
     * B是A的子类，因此funA(b)方法中可以传入b
     * 但是List<B> 与 List<A>不是父子类的关系（这里隐式转换不了），因此方式传参是有问题的
     */
    //class A{}
    //class B extends A {}
    //
    //    // 如下两个方法不会报错
    //    public static void funA(A a) {
    //        // ...
    //    }
    //    public static void funB(B b) {
    //        funA(b);
    //        // ...
    //    }
    //
    //    // 如下funD方法会报错
    //    public static void funC(List<A> listA) {
    //        // ...
    //    }
    //    public static void funD(List<B> listB) {
    //        funC(listB); // Unresolved compilation problem: The method doPrint(List<A>) in the type test is not applicable for the arguments (List<B>)
    //        // ...
    //    }


    /**
     **   <? extends A> 表示该类型的参数可以是A上边界或者A的子类类型
     */
//    public static void funC(List<? extends A> listA) {
//        // ...
//    }
//    public static void funD(List<B> listB) {
//        funC(listB); // OK
//        // ...
//    }


    /**
     * <?> 无限制通配符
     * <? extends E> extends 关键字声明了类型的上界，表示参数化的类型可能是所指定的类型，或者是此类型的子类
     * <? super E> super 关键字声明了类型的下界，表示参数化的类型可能是指定的类型，或者是此类型的父类
     *
     * // 使用原则《Effictive Java》
     * // 为了获得最大限度的灵活性，要在表示 生产者或者消费者 的输入参数上使用通配符，使用的规则就是：生产者有上限、消费者有下限
     * 1. 如果参数化类型表示一个 T 的生产者，使用 < ? extends T>;
     * 2. 如果它表示一个 T 的消费者，就使用 < ? super T>；
     * 3. 如果既是生产又是消费，那使用通配符就没什么意义了，因为你需要的是精确的参数类型。
     */

    public static void main2(String args[]){

        // 上限
        InfoA<Integer> i1 = new InfoA<Integer>() ;        // 声明Integer的泛型对象

        // 下限
        InfoB<String> i1b = new InfoB<String>() ;        // 声明String的泛型对象
        InfoB<Object> i2 = new InfoB<Object>() ;        // 声明Object的泛型对象
        i1b.setVar("hello") ;
        i2.setVar(new Object()) ;
        fun(i1b) ;
        fun(i2) ;
    }

    public static void fun(InfoB<? super String> temp){    // 只能接收String或Object类型的泛型，String类的父类只有Object类
        System.out.print(temp + ", ") ;
    }

    static class InfoA<T extends Number>{    // 此处泛型只能是数字类型
        private T var ;        // 定义泛型变量
        public void setVar(T var){
            this.var = var ;
        }
        public T getVar(){
            return this.var ;
        }
        public String toString(){    // 直接打印
            return this.var.toString() ;
        }
    }


    static class InfoB<T>{
        private T var ;        // 定义泛型变量
        public void setVar(T var){
            this.var = var ;
        }
        public T getVar(){
            return this.var ;
        }
        public String toString(){    // 直接打印
            return this.var.toString() ;
        }
    }


    /**
     *
     * 看个特殊的理解下
     *
     * 类型参数E的范围 <E extends Comparable<? super E>>
     *      1.要求E是可以比较的类，因此需要extends Comparable<...>
     *      2.Comparable<? super E>  需要对E进行比较，即E的消费者，所以需要使用super
     *      3.参数List<? extends E> e1表示要操作的数据是E的子类列表，指定上限
     * @param e1
     * @param <E>
     * @return
     */
    private  <E extends Comparable<? super E>> E max(List<? extends E> e1) {
        if (e1 == null){
            return null;
        }
        //迭代器返回的元素属于 E 的某个子类型
        Iterator<? extends E> iterator = e1.iterator();
        E result = iterator.next();
        while (iterator.hasNext()){
            E next = iterator.next();
            if (next.compareTo(result) > 0){
                result = next;
            }
        }
        return result;
    }

    /**
     * 泛型数组
     */

//    List<String>[] list11 = new ArrayList<String>[10]; //编译错误，非法创建
//    List<String>[] list12 = new ArrayList<?>[10]; //编译错误，需要强转类型
//    List<String>[] list13 = (List<String>[]) new ArrayList<?>[10]; //OK，但是会有警告
//    List<?>[] list14 = new ArrayList<String>[10]; //编译错误，非法创建
//    List<?>[] list15 = new ArrayList<?>[10]; //OK
//    List<String>[] list6 = new ArrayList[10]; //OK，但是会有警告

        public static void main3(String args[]){
            Integer i[] = fun1(1,2,3,4,5,6) ;   // 返回泛型数组
            fun2(i) ;
        }

        public static <T> T[] fun1(T...arg){  // 接收可变参数
            return arg ;            // 返回泛型数组
        }
        public static <T> void fun2(T param[]){   // 输出
            System.out.print("接收泛型数组：") ;
            for(T t:param){
                System.out.print(t + "、") ;
            }
        }


}




