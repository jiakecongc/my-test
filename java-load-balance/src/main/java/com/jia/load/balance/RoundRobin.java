package com.jia.load.balance;

public class RoundRobin {

    private static Integer pos = 0;

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(getServer());
        }
    }


    /**
     * 轮询算法， 就是根据轮询到的位置进行下标的选择即可
     * @return
     */
    public static String getServer() {
        String ip = null;
        synchronized (pos) {
            if (pos > ServerIps.ipList.size()) {
                pos = 0;
            }
            ip = ServerIps.ipList.get(pos);
            pos++;
        }


        return ip;
    }
}
