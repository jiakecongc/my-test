package com.jia.load.balance;

import java.util.HashMap;
import java.util.Map;

public class WeightRoundBobin {
    
    
    private static Map<String, Weight> weightMap = new HashMap<>();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(getServer());
        }
    }
    
    public static String getServer() {
        int totalWeight = ServerIps.ipWithWeight.values().stream().reduce(0, (w1, w2) -> w1+w2);
        
        if (weightMap.isEmpty()) {
            ServerIps.ipWithWeight.forEach((key, value) -> {
                weightMap.put(key, new Weight(key, value, value));
            });
        }

        // 查找出当前权重最大的ip结点。根据运行次数不同，权重会进行更改，因此就会轮询
        Weight maxCurrentWeight = null;
        for (Weight weight: weightMap.values()) {
            if (maxCurrentWeight == null || weight.getCurrentWeight() > maxCurrentWeight.getCurrentWeight()) {
                maxCurrentWeight = weight;
            }
        }

        // 每轮过后，将自己的权重降低。下次轮询的时候，可能就会更换结点了
        maxCurrentWeight.setCurrentWeight(maxCurrentWeight.getCurrentWeight() - totalWeight);

        for (Weight weight : weightMap.values()) {
            weight.setCurrentWeight(weight.getCurrentWeight() + weight.getWeight());
        }

        return maxCurrentWeight.getIp();
        
    }


    
    
}



class Weight {
    private String ip;
    private Integer weight;
    private Integer currentWeight;


    public Weight(String ip, Integer weight, Integer currentWeight) {
        this.ip = ip;
        this.weight = weight;
        this.currentWeight = currentWeight;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(Integer currentWeight) {
        this.currentWeight = currentWeight;
    }
}
