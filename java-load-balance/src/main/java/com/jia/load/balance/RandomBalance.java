package com.jia.load.balance;

import java.util.Random;

public class RandomBalance {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(getServer());
        }
    }


    /**
     * 随机算法，可以通过随机产生一个随机数，根据随机数获取到具体的serverIp。
     * 缺点：如果随机数比较集中，可能导致多数的请求落到同一台机器上面
     * @return
     */
    public static String getServer() {
        Random random = new Random();
        int i = random.nextInt(ServerIps.ipList.size());
        return ServerIps.ipList.get(i);
    }



}
