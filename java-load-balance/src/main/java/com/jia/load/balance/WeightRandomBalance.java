package com.jia.load.balance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeightRandomBalance {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
//            System.out.println(getServer());
            System.out.println(getServer2());
        }
    }


    /**
     * 根据权重进行随机。方式就是权重占比比较大的服务器，在每次生成的随机数去list#get(i)查找的时候，更有可能被查找到
     *
     * 缺点：遇到权重之和特别大的时候比较容易消耗内存，因为需要对ip地址进行复制。权重之和越大，需复制的份数越多
     * @return
     */
    public static String getServer() {
        // 按照权重添加服务器到list中，权重越大，list中占有的数据就越多
        List<String> ips = new ArrayList<>();
        for (String ip : ServerIps.ipWithWeight.keySet()) {
            Integer weight = ServerIps.ipWithWeight.get(ip);
            for (int i = 0; i < weight; i++) {
                ips.add(ip);
            }
        }

        Random random = new Random();
        int i = random.nextInt(ips.size());
        return ips.get(i);

    }


    /**
     * 解决上面的权重之和越大的问题
     * 其实可以理解为 server=[A B C] weight = [5, 3, 2] 那么A:[0, 5),B:[5, 8),C:[8, 10)
     * @return
     */
    public static String getServer2() {
        int allWeight = 0;
        // 如果所有的权重都一致，那么其实就无关调用哪台服务器了
        boolean ifSameWeight = true;
        Object[] objects = ServerIps.ipWithWeight.values().toArray();
        for (int i = 0; i < objects.length; i++) {
            Integer weight = (Integer) objects[i];
            allWeight += weight;
            // 只要与前一个服务器的权重不一致，那么就修改权重标识
            if (ifSameWeight && i > 0 && !weight.equals(objects[i-1])) {
                ifSameWeight = false;
            }
        }

        Random random = new Random();
        // 在所有的权重之和的情况下随机产生数字
        int randomIndex = random.nextInt(allWeight);
        if (!ifSameWeight) {
            // 这里就是去判断权重是落在哪个区间上面去，写法会有点绕
            // 如果 randomIndex = 7
//            randomIndex < 5 ? false   randomIndex - value = 2
//            randomIndex < 3 ? true  就是在这个区间B:[5, 8)
            for (String ip : ServerIps.ipWithWeight.keySet()) {
                Integer value = ServerIps.ipWithWeight.get(ip);
                if (randomIndex < value) {
                    return ip;
                }
                randomIndex -= value;
            }
        }
        // 没有则随机返回
        return (String) ServerIps.ipWithWeight.keySet().toArray() [new Random().nextInt(ServerIps.ipWithWeight.size())];
    }

}
