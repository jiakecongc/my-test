package com.jia.load.balance;

import java.util.SortedMap;
import java.util.TreeMap;

public class ConsistentHash {


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(getServer("client" + i));
        }
    }

    private static String getServer(String client) {
        int hash = getHash(client);
        SortedMap<Integer, String> subMap = virturalNodes.tailMap(hash);
        Integer nodeIndex = subMap.firstKey();
        if (nodeIndex == null) {
            nodeIndex = virturalNodes.firstKey();
        }
        return subMap.get(nodeIndex);
    }

    private static SortedMap<Integer, String> virturalNodes = new TreeMap<>();

    private static final int VIRTUAL_NODES = 160;

    // 生成虚拟结点
    static {
        for (String ip : ServerIps.ipList) {
            for (int i = 0; i < VIRTUAL_NODES; i++) {
                int hash = getHash(ip + "VN" + i);
                virturalNodes.put(hash, ip);
            }
        }
    }



    private static int getHash(String str) {
        final int p = 16777619;
        int hash = (int) 2166136261L;
        for (int i = 0; i < str.length(); i++) {
            hash = (hash ^ str.charAt(i)) * p;
        }
        hash += hash << 13;
        hash ^= hash >> 7;
        hash += hash << 3;
        hash ^= hash >> 17;
        hash += hash << 5;

        if (hash < 0) {
            hash = Math.abs(hash);
        }

        return hash;
    }


}
