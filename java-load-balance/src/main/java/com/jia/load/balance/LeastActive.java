package com.jia.load.balance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class LeastActive {

    public static final Map<String, Integer> ACTIVITY_LIST = new LinkedHashMap<>();
    static {
        // 最小活跃数越小，说明服务器处理速度越快
        //  每个服务提供者对应一个活跃数。初始化的情况下，所有活跃数都是0，有调用的时候加1， 调用结束就减1.
        //  因此每个服务处理的速度越快，那么这个活跃数下降得越快
        ACTIVITY_LIST.put("192.168.0.1", 1);
        ACTIVITY_LIST.put("192.168.0.2", 2);
        ACTIVITY_LIST.put("192.168.0.3", 3);
        ACTIVITY_LIST.put("192.168.0.4", 4);
        ACTIVITY_LIST.put("192.168.0.5", 5);
        ACTIVITY_LIST.put("192.168.0.6", 6);
        ACTIVITY_LIST.put("192.168.0.7", 7);
        ACTIVITY_LIST.put("192.168.0.8", 8);
        ACTIVITY_LIST.put("192.168.0.9", 9);
        ACTIVITY_LIST.put("192.168.0.10", 10);
    }


    public static String getServer() {
        // 找出当前活跃数最小的ip
        Optional<Integer> min = ACTIVITY_LIST.values().stream().min(Comparator.naturalOrder());
        if (min.isPresent()) {
            List<String> minActivityIps = new ArrayList<>();
            ACTIVITY_LIST.forEach((ip, activity) -> {
                if (activity.equals(min.get())) {
                    minActivityIps.add(ip);
                }
            });

            // 如果最小活跃数的ip有多个，则根据权重来选择
            if (minActivityIps.size() > 1) {
                Map<String, Integer> weightList = new LinkedHashMap<>();
                ServerIps.ipWithWeight.forEach((ip, weight) -> {
                    if (minActivityIps.contains(ip)) {
                        weightList.put(ip, ServerIps.ipWithWeight.get(ip));
                    }
                });

                int totalWeight = 0;
                boolean sameWeight = true;

                Object[] objects = weightList.values().toArray();
                for (int i = 0; i < objects.length; i++) {
                    Integer weight = (Integer) objects[i];
                    totalWeight += weight;
                    if (sameWeight && i > 0 && !objects.equals(objects[i-1])) {
                        sameWeight = false;
                    }
                }

                Random random = new Random();
                int i = random.nextInt(totalWeight);
                if (!sameWeight) {
                    for (String ip : weightList.keySet()) {
                        Integer value = weightList.get(ip);
                        if (i < value) {
                            return ip;
                        }
                        i -= value;
                    }
                }
                return (String) weightList.keySet().toArray()[new Random().nextInt(weightList.size())];
            } else {
                return minActivityIps.get(0);
            }
        } else {
            return (String) ServerIps.ipWithWeight.keySet().toArray()[new Random().nextInt(ServerIps.ipWithWeight.size())];
        }
    }


    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(getServer());
        }
    }


}
