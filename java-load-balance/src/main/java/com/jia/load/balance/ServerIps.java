package com.jia.load.balance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerIps {

    /**
     * 无权重的IP
     */
    public static List<String> ipList = Arrays.asList(
        "192.168.0.1",
        "192.168.0.2",
        "192.168.0.3",
        "192.168.0.4",
        "192.168.0.5",
        "192.168.0.6",
        "192.168.0.7",
        "192.168.0.8",
        "192.168.0.9",
        "192.168.0.10"
            );


    /**
     * 带权重的ip
     */
    public static Map<String, Integer> ipWithWeight = new HashMap<>();
    static {
//        ipWithWeight.put("192.168.0.1", 1);
//        ipWithWeight.put("192.168.0.2", 2);
//        ipWithWeight.put("192.168.0.3", 3);
//        ipWithWeight.put("192.168.0.4", 4);
//        ipWithWeight.put("192.168.0.5", 5);
//        ipWithWeight.put("192.168.0.6", 6);
//        ipWithWeight.put("192.168.0.7", 7);
//        ipWithWeight.put("192.168.0.8", 8);
//        ipWithWeight.put("192.168.0.9", 9);
//        ipWithWeight.put("192.168.0.10", 10);


        ipWithWeight.put("192.168.0.1", 5);
        ipWithWeight.put("192.168.0.2", 1);
        ipWithWeight.put("192.168.0.3", 1);
    }

}
