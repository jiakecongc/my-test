package com.jia.springboot.mybatisplus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTestMybatisPlusApplication.class)
public class MyTestMybatisPlusApplicationTests {

    @Test
    public void contextLoads() {
    }

}
