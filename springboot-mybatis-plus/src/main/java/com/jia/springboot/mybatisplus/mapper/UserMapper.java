package com.jia.springboot.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jia.springboot.mybatisplus.entity.User;
import org.springframework.stereotype.Component;

/**
 * <p>
 * UserMapper
 * </p>
 *
 */
@Component
public interface UserMapper extends BaseMapper<User> {
}
