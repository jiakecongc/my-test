package com.jia.springboot.mybatisplus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动器
 * </p>
 *
 */
@MapperScan(basePackages = {"com.jia.springboot.mybatisplus.mapper"})
@SpringBootApplication
public class MyTestMybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestMybatisPlusApplication.class, args);
    }
}
