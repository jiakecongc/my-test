package com.jia.springboot.mybatisplus.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jia.springboot.mybatisplus.entity.Role;

/**
 * <p>
 * RoleMapper
 * </p>
 *
 */
public interface RoleMapper extends BaseMapper<Role> {
}
