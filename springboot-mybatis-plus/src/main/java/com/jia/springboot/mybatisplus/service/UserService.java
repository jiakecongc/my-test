package com.jia.springboot.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jia.springboot.mybatisplus.entity.User;

/**
 * <p>
 * User Service
 * </p>
 *
 */
public interface UserService extends IService<User> {
}
