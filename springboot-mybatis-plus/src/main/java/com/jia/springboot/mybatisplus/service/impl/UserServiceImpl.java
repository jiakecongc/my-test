package com.jia.springboot.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jia.springboot.mybatisplus.entity.User;
import com.jia.springboot.mybatisplus.mapper.UserMapper;
import com.jia.springboot.mybatisplus.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * User Service
 * </p>
 *
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
