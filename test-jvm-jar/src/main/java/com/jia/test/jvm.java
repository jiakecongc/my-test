package com.jia.test;

import java.util.ArrayList;
import java.util.List;

public class jvm {

    public static void main(String[] args) {
        int max = 100000;

        List<String> bigString = new ArrayList<>();
        for (int i = 0; i < max; i++) {
            bigString.add("jiakecongc" + i);
        }

        Object a = new Object();
        Object b = new Object();

        new Thread(() -> {
            try {
                synchronized (a) {
                    Thread.sleep(1000);
                    synchronized (b) {
                        System.out.println("jiakecongc test a");
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int i = 5 / 0;
            System.out.println("jiakecongc test a threat fail");
        }).start();

        new Thread(() -> {
            try {
                synchronized (b) {
                    Thread.sleep(1000);
                    synchronized (a) {
                        System.out.println("jiakecongc test b");
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int i = 5 / 0;
            System.out.println("jiakecongc test b threat fail");
        }).start();

        System.out.println("jiakecongc test jvm");

        List<Object> list = new ArrayList<>();
        int i = 0;
        int j = 0;
        while (true) {
//            list.add(new User(i++, UUID.randomUUID().toString()));
//            new User(j--, UUID.randomUUID().toString());
            compute();

        }
    }

    public static int compute() {
        int a = 1;
        int b = 2;
        return a + b;
    }

}
