package com.jia.springboot.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动器
 * </p>
 *
 */
@SpringBootApplication
public class MyTestSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestSwaggerApplication.class, args);
    }
}
