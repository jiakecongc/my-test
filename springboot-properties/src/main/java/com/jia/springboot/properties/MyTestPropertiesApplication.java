package com.jia.springboot.properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@SpringBootApplication
public class MyTestPropertiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestPropertiesApplication.class, args);
    }
}
