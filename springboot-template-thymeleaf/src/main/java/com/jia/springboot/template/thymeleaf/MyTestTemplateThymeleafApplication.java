package com.jia.springboot.template.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@SpringBootApplication
public class MyTestTemplateThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestTemplateThymeleafApplication.class, args);
    }
}
