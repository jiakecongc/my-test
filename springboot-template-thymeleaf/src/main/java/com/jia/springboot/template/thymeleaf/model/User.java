package com.jia.springboot.template.thymeleaf.model;

import lombok.Data;

/**
 * <p>
 * 用户 model
 * </p>
 *
 */
@Data
public class User {
    private String name;
    private String password;
}
