package com.jia.test.ioc.testbeanpostpostprocessor;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by smlz on 2019/5/31.
 */
public class MainClass {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);

        Object compentA = context.getBean("compentA");
        System.out.println(compentA);

        TulingLog tulingLog = context.getBean(TulingLog.class);
        tulingLog.print();

        /**
         * TulingAspect交给了spring容器进行管理，可以调通
         */
        TulingAspect tulingAspect = context.getBean(TulingAspect.class);
        tulingAspect.invokeTulingLogPrint();

        /**
         * ApplicationContextAware 通过它Spring容器会自动把上下文环境对象调用ApplicationContextAware接口中的setApplicationContext方法。
         * 实现了这个接口的bean，当spring容器初始化的时候，会自动的将ApplicationContext注入进来.
         * 测试了下可以知道TulingAspect2需要被spring容器进行管理
         */
        TulingAspect2.invokeTulingLogPrint();

        context.close();
    }
}
