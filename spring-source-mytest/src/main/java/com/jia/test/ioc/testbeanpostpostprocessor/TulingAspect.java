package com.jia.test.ioc.testbeanpostpostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Created by smlz on 2019/6/2.
 */
@Component
public class TulingAspect implements ApplicationContextAware{

    /**
     * ApplicationContextAware 通过它Spring容器会自动把上下文环境对象调用ApplicationContextAware接口中的setApplicationContext方法。
     * 实现了这个接口的bean，当spring容器初始化的时候，会自动的将ApplicationContext注入进来.
     */

    private ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void invokeTulingLogPrint() {
        TulingLog tulingLog = applicationContext.getBean(TulingLog.class);
        tulingLog.print();
    }
}
