package com.jia.test.ioc.testlookup;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by smlz on 2019/6/6.
 */
@Configuration
@ComponentScan(basePackages = {"com.jia.test.ioc.testlookup"})
public class MainConfig {


}
