package com.jia.test.ioc.testdependsOn;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by smlz on 2019/6/13.
 */
public class MainClsss {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MainConfig.class);
        Object dependsA = context.getBean("dependsA");
        System.out.println(dependsA);
    }
}
