package com.jia.test.ioc.circulardependencies;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by smlz on 2019/5/29.
 */
@Configuration
@ComponentScan(basePackages = {"com.jia.test.ioc.circulardependencies"})
public class MainConfig {

    /**
     * 1.正常来说，setter或者field注入的方式可以正常交由spring的三级缓存来解决，说是三级缓存，其实就是三个map，因为map和map之间并不是递进的关系
     * 所以还是建议使用三个map的说法而不是三级缓存，因为缓存我们一般理解是有递进的关系的。
     *
     *
     * 2.构造器的方式注入，可以通过@Lazy注解来解决循环依赖的问题其基本思路是：对于强依赖的对象，一开始并不注入对象本身，而是注入其代理对象，
     * 以便顺利完成实例的构造，形成一个完整的对象，这样与其它应用层对象就不会形成互相依赖的关系；当需要调用真实对象的方法时，
     * 通过TargetSource去拿到真实的对象[DefaultListableBeanFactory#doResolveDependency]，然后通过反射完成调用。
     */
//  @Bean
//    public InstanceA instanceA(@Lazy InstanceB instanceB){
//        return new InstanceA(instanceB);
//    }
//
//    @Bean
//    public InstanceB instanceB(@Lazy InstanceA instanceA) {
//        return new InstanceB(instanceA);
//    }
}
