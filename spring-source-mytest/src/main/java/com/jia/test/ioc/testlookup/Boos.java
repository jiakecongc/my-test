package com.jia.test.ioc.testlookup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

/**
 * Created by smlz on 2019/6/6.
 */
@Component
public class Boos {

    @Autowired
    private Car car;


    @Override
    public String toString() {
        return "Boos{" +
                "car=" + car +
                '}';
    }

    /**
     * @Lookup  作用在方法上的注解，被其标注的方法会被重写，然后根据其返回值的类型，容器调用BeanFactory的getBean()方法来返回一个bean
     * 如果不加这个注解，那么Boos对象的Car属性都是同一个对象，这是因为Boos是一个单例Bean，属性注入的机会只有一次，那么每次获取的都是都一个Car对象，尽管Car是一个多例Bean。
     */
    @Lookup
    public Car getCar() {
        System.out.println("我要从容器中获取");
//        return null;
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

}
