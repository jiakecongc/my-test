package com.jia.test.ioc.testdependsOn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

/**
 * Created by smlz on 2019/6/13.
 */
@Configuration
public class MainConfig {




    @Bean
    public DependsA dependsA() {
        return new DependsA();
    }

    /**
     * dependsB的加载必须依赖dependsA,否则将不会加载 dependsB，因此可以控制dependsB在dependsA的后面才会初始化
     */
    @Bean
    @DependsOn(value = {"dependsA"})
    public DependsB dependsB() {
        return new DependsB();
    }
}
