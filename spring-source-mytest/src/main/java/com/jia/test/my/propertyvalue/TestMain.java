package com.jia.test.my.propertyvalue;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;

/**
 */
public class TestMain {
	public static void main(String[] args) {
		// PropertySource将外部的配置文件中的key/value解析出来之后，会放入到当前容器运行的环境变量中.
		ApplicationContext acx = new AnnotationConfigApplicationContext(ConfigOfPropertyValues.class);
		Object person = acx.getBean("person");
		// 从当前环境中获取properties中的值.
		Environment environment = acx.getEnvironment();
		String nickName = environment.getProperty("person.nick");
		System.out.println("nick: " + nickName);

		System.out.println(person);
	}
}
