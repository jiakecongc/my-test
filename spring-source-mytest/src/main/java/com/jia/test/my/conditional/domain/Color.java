package com.jia.test.my.conditional.domain;

/**
 */
public class Color {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Color{" +
				"name='" + name + '\'' +
				'}';
	}
}
