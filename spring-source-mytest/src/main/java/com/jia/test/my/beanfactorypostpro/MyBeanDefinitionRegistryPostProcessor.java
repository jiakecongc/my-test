package com.jia.test.my.beanfactorypostpro;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.stereotype.Component;


/**
 *  *  BeanFactoryPostProcessor  bean工厂后置处理器
 *  *  BeanDefinitionRegistryPostProcessor  bean定义注册后置处理器，继承了BeanFactoryPostProcessor。
 *  *      执行过程：
 *  *          执行时先执行BeanDefinitionRegistryPostProcessor的postProcessBeanDefinitionRegistry注册Bean定义后置方法
 *  *          接着执行BeanDefinitionRegistryPostProcessor的postProcessBeanFactory方法
 *  *          最后才执行BeanFactoryPostProcessor的postProcessBeanFactory方法
 */
@Component
public class MyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("MyBeanDefinitionRegistryPostProcessor ...postProcessBeanFactory... bean的数量为:" + beanFactory.getBeanDefinitionCount());
	}

	/**
	 * bean定义信息的保存中心
	 *
	 * BeanFactory就是根据BeanDefinitionRegistry中保存的每一个Bean定义去创建bean示例的.
	 *
	 * @param registry the bean definition registry used by the application context
	 * @throws BeansException
	 */
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		System.out.println("MyBeanDefinitionRegistryPostProcessor ...postProcessBeanDefinitionRegistry... bean的数量为:" + registry.getBeanDefinitionCount());
//		RootBeanDefinition beanDefinition = new RootBeanDefinition(Blue.class);
		AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.rootBeanDefinition(Blue.class).getBeanDefinition();
		registry.registerBeanDefinition("customerBlue", beanDefinition);
	}
}
