package com.jia.test.my.conditional.config;

import com.jia.test.my.conditional.LinuxConditional;
import com.jia.test.my.conditional.domain.LinuxCalc;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.jia.test.my.conditional")
public class ConditionalConfig {


	/**
	 * LinuxConditional 这里限定了只有在window的环境下，这个类才生效
	 * 测试的注解为@Conditional
	 */
	@Bean("linuxCalc")
	@Conditional(value = LinuxConditional.class)
	public LinuxCalc calc() {
		return new LinuxCalc();
	}
}
