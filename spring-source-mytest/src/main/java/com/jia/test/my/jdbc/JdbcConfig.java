package com.jia.test.my.jdbc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
@Configuration
public class JdbcConfig {

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://121.5.187.65:3306/ms_user?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
		dataSource.setUsername("guest");
		dataSource.setPassword("guest");
		return dataSource;
	}
}
