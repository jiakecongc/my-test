package com.jia.test.my.listener;

import org.springframework.context.ApplicationEvent;

public class MyEvent extends ApplicationEvent {
	private static final long serialVersionUID = 1L;

	public MyEvent(Object source) {
		super(source);
//		System.out.println("test My event");
	}
}
