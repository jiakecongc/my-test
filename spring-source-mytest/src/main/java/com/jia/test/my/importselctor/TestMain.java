package com.jia.test.my.importselctor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 */
public class TestMain {

	public static void main(String[] args) {

		ApplicationContext acx = new AnnotationConfigApplicationContext(BeanConfig.class);


//		@Import(value = {InstC.class}) // 注入普通的组件
//@Import(value = {com.jia.test.my.importselctor.MyImportSelector.class})
//		使用ImportSelector及直接import类名的方式，生成的备案Name都是全限定类名
		InstC obj = (InstC) acx.getBean("com.jia.test.my.importselctor.InstC");

//@Import(value = {MyImportBeanDefinitionRegistrar.class}) // 导入的组件实现了ImportBeanDefinitionRegistrar接口
//		使用ImportBeanDefinitionRegistrar 的方式，是在registerBeanDefinitions方法中定义具体的beanName
//		InstC obj = (InstC) acx.getBean("instC");
		System.out.println(obj);
	}
}
