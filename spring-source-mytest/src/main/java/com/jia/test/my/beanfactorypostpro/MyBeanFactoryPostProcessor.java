package com.jia.test.my.beanfactorypostpro;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;


/**
 *  *  BeanFactoryPostProcessor  bean工厂后置处理器
 *  *  BeanDefinitionRegistryPostProcessor  bean定义注册后置处理器，继承了BeanFactoryPostProcessor。
 *  *      执行过程：
 *  *          执行时先执行BeanDefinitionRegistryPostProcessor的postProcessBeanDefinitionRegistry注册Bean定义后置方法
 *  *          接着执行BeanDefinitionRegistryPostProcessor的postProcessBeanFactory方法
 *  *          最后才执行BeanFactoryPostProcessor的postProcessBeanFactory方法
 */
@Component
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("postProcessBeanFactory...");
		int count = beanFactory.getBeanDefinitionCount();
		System.out.println("已经被加载的bean定义的个数为:" + count);
		System.out.println("已经加载的bean的名称如:");
		String[] beanNames = beanFactory.getBeanDefinitionNames();
		for (String beanName : beanNames) {
			System.out.println(beanName);
		}
	}
}
