package com.jia.test.my.listener;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestMain {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext acx = new AnnotationConfigApplicationContext(ExtConfig.class);
		// 发布一个自定义事件.
		acx.publishEvent(new MyEvent("test"));
		// 容器关闭
		acx.close();
	}
}