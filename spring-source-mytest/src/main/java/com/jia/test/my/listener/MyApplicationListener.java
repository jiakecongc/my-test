package com.jia.test.my.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MyApplicationListener implements ApplicationListener<MyEvent> {
	/**
	 * 当容器中发布事件之后，会触发该方法
	 * @param event the event to respond to
	 */
	@Override
	public void onApplicationEvent(MyEvent event) {
		System.out.println("收到事件:" + event);
	}
}
