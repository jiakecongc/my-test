package com.jia.test.my.conditional.domain;

public class Sheep {
	private Long id;
	private String name;
	private Color color;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public String toString() {
		return "Sheep{" +
				"id=" + id +
				", name='" + name + '\'' +
				", color=" + color +
				'}';
	}
}
