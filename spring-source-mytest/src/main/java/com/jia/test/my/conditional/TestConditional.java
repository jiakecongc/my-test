package com.jia.test.my.conditional;

import com.jia.test.my.conditional.config.ConditionalConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestConditional {
	public static void main(String[] args) {
		ApplicationContext acx = new AnnotationConfigApplicationContext(ConditionalConfig.class);
		String[] names = acx.getBeanDefinitionNames();
		for (String name : names) {
			System.out.println(name);
		}
	}
}
