package com.jia.test.my.beanfactorypostpro;

/**
 */
public class Blue {
	private String colorName;

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	@Override
	public String toString() {
		return "Blue{" +
				"colorName='" + colorName + '\'' +
				'}';
	}
}
