package com.jia.test.my.importselctor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "com.jia.test.my.importselctor")
@Import(value = {InstC.class}) // 注入普通的组件
//@Import(value = {MyImportBeanDefinitionRegistrar.class}) // 导入的组件实现了ImportBeanDefinitionRegistrar接口
//@Import(value = {com.jia.test.my.importselctor.MyImportSelector.class})
public class BeanConfig {

//	@Bean
//	@Lazy // Spring容器启动的时候不会初始化，第一次获取Bean的时候才会去初始化.
//	public UserBean userBean() {
//		UserBean user = new UserBean();
//		user.setUuid(UUID.randomUUID().toString());
//		System.out.println("properties set finished...");
//		return user;
//	}
}
