package com.jia.test.my.beanfactorypostpro;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 */
public class TestMain {

	/**
	 * MyBeanDefinitionRegistryPostProcessor ...postProcessBeanDefinitionRegistry... bean的数量为:9
	 * MyBeanDefinitionRegistryPostProcessor ...postProcessBeanFactory... bean的数量为:10
	 * postProcessBeanFactory...
	 * 已经被加载的bean定义的个数为:10
	 * 已经加载的bean的名称如:
	 * org.springframework.context.annotation.internalConfigurationAnnotationProcessor
	 * org.springframework.context.annotation.internalAutowiredAnnotationProcessor
	 * org.springframework.context.annotation.internalRequiredAnnotationProcessor
	 * org.springframework.context.annotation.internalCommonAnnotationProcessor
	 * org.springframework.context.event.internalEventListenerProcessor
	 * org.springframework.context.event.internalEventListenerFactory
	 * extConfig
	 * myBeanDefinitionRegistryPostProcessor
	 * myBeanFactoryPostProcessor
	 * customerBlue
	 */
	public static void main(String[] args) {
		AnnotationConfigApplicationContext acx = new AnnotationConfigApplicationContext(ExtConfig.class);
		acx.close();
	}
}