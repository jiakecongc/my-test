package com.jia.test.my.beanpostpro;

import javax.annotation.PostConstruct;

/**
 */
public class Pig {
	private String id;
	private String name;

	public Pig() {
		System.out.println("开始创建Pig实例...");
	}

	@PostConstruct
	public void init() {
		System.out.println("Pig... PostConstruct...");
	}

	public Pig(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Pig{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				'}';
	}
}
