package com.jia.test.my.propertyvalue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

// PropertySource将外部的配置文件中的key/value解析出来之后，会放入到当前容器运行的环境变量中.
@PropertySource(value = {"classpath:public.properties"},encoding = "UTF-8")
@Configuration
@ComponentScan(basePackages = "com.jia.test.my.propertyvalue")
public class ConfigOfPropertyValues {

	@Bean
	public Person person() {
		return new Person();
	}
}
