package com.jia.test.my.listener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class UserService {




	@EventListener(classes = {MyEvent.class})
	public void listen(ApplicationEvent event) {
		System.out.println("UserService收到的事件1:" + event);
	}


	@EventListener(classes = {ApplicationEvent.class})
	public void listen2(ApplicationEvent event) {
		System.out.println("UserService收到的事件2:" + event);
	}




}
