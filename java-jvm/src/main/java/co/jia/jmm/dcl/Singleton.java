package co.jia.jmm.dcl;


public class Singleton {

    // 构造器私有，禁止外部new
    private Singleton() {
    }

    /**
     * 查看汇编指令
     * -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly -Xcomp
     */
    private volatile static Singleton myinstance;

    /**
     * 双重锁机制保证单例安全,除非是反射机制，否则很安全了 volatile修饰
     * @return
     */
    public static Singleton getInstance() {
        if (myinstance == null) {
            synchronized (Singleton.class) {
                if (myinstance == null) {
                    myinstance = new Singleton();
                }
            }
        }
        return myinstance;
    }

    public static void main(String[] args) {
        Singleton instance = Singleton.getInstance();
    }
}
