//package java.lang;
//
//public class String {
//
//    public static void main(String[] args) {
//        //错误: 在类 java.lang.String 中找不到 main 方法, 请将 main 方法定义为:
//        //   public static void main(String[] args)
//        //否则 JavaFX 应用程序类必须扩展javafx.application.Application
//        //实际String是由bootstrapClassLoader加载了，是Java写的String类，不是自己写的，因此没有main方法
//        System.out.println("--------------双亲委派就派上用场了-----------");
//    }
//}
