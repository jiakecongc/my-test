package com.jia.jvm;

import sun.misc.Launcher;

import java.net.URL;

public class TestJDKClassLoader {

    public static void main(String[] args) {
        // 引导类加载器是C++实现的，Java这里打印出来的是null
        System.out.println(String.class.getClassLoader());
        System.out.println(com.sun.crypto.provider.DESKeyFactory.class.getClassLoader().getClass().getName());
        System.out.println(TestJDKClassLoader.class.getClassLoader().getClass().getName());


        System.out.println("========================");
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader(); // appClassLoader
        ClassLoader systemClassLoaderParent = systemClassLoader.getParent(); // extClassLoader
        ClassLoader systemClassLoaderParentParent = systemClassLoaderParent.getParent(); // bootstrapLoader

        //appClassLoader: sun.misc.Launcher$AppClassLoader@18b4aac2
        //extClassLoader: sun.misc.Launcher$ExtClassLoader@38af3868
        //bootstrapLoader: null
        System.out.println("appClassLoader: " + systemClassLoader);
        System.out.println("extClassLoader: " + systemClassLoaderParent);
        System.out.println("bootstrapLoader: " + systemClassLoaderParentParent);


        System.out.println("===============bootstrapLoader加载的路径=====");
        URL[] urLs = Launcher.getBootstrapClassPath().getURLs();
        for(URL url : urLs) {
            System.out.println(url);
        }

        System.out.println("===============extClassLoader加载的路径=====");
        System.out.println(System.getProperty("java.ext.dirs"));


        System.out.println("===============appClassLoader加载的路径=====");
        System.out.println(System.getProperty("java.class.path"));



    }






}
