package com.jia.jvm;

import java.io.FileInputStream;
import java.lang.reflect.Method;

public class MyClassLoaderTest {

    static class MyClassLoader extends ClassLoader {
        private String classPath;

        public MyClassLoader (String classPath) {
            this.classPath = classPath;
        }

        private byte[] loadByte(String name) throws Exception {
            name = name.replaceAll("\\.", "/");
            FileInputStream fis = new FileInputStream(classPath + "/" + name + ".class");
            int len = fis.available();
            byte[] data = new byte[len];
            fis.read(data);
            fis.close();
            return data;
        }

        // 继承ClassLoader并重写findClass方法，就可以自定义类加载器
        protected Class<?> findClass(String name) throws ClassNotFoundException {
            try {
                byte[] data = loadByte(name);
                // defineClass将一个字节数组转为Class对象， 这个字节数组是class问价读取后的字节数组
                return defineClass(name, data, 0, data.length);
            } catch (Exception e) {
                e.printStackTrace();
                throw new ClassNotFoundException();
            }
        }


        /**
         * 重写类加载方法，实现自己的加载逻辑，不委派给双亲加载，这样就可以打破双亲加载机制
         * @param name
         * @param resolve
         * @return
         * @throws ClassNotFoundException
         */
        protected Class<?> loadClass(String name, boolean resolve)
                throws ClassNotFoundException
        {
            synchronized (getClassLoadingLock(name)) {
                // First, check if the class has already been loaded
                Class<?> c = findLoadedClass(name);
                if (c == null) {
                    // If still not found, then invoke findClass in order
                    // to find the class.
                    long t1 = System.nanoTime();
                    // 如果时以自己定义的路径，那么设置为按照自己的类加载器加载，否则双亲委派
                    if (name.startsWith("com.jia.jvm")) {
                        c = findClass(name);
                    } else {
                        c = this.getParent().loadClass(name);
                    }

                    // this is the defining class loader; record the stats
                    sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
                    sun.misc.PerfCounter.getFindClasses().increment();
                }
                if (resolve) {
                    resolveClass(c);
                }
                return c;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        // 初始化自定义类加载器，会先初始化父类Class Loader，其中会把自定义类加载器的父加载器设置为应用程序类加载器AppClassLoader
        MyClassLoader classLoader = new MyClassLoader("D:/Test");
        Class clazz = classLoader.loadClass("com.jia.jvm.Test");
        Object newInstance = clazz.newInstance();
        Method main = clazz.getDeclaredMethod("sout", null);
        main.invoke(newInstance, null);
        // 这里的类加载器为MyClassLoader 注意：上面的类不能在AppClassLoader加载的路径下：classpath下，否则会被AppClassLoader加载，而不是自己的自定义加载器加载
        System.out.println(clazz.getClassLoader().getClass().getName());


        // 同一个JVM内，两个相同包名和类名的类对象可以共存，因为他们的类加载器可以不一样。所以看两个
        // 类对象是否同一个，除了看类的包名和类名是否都相同之外，还需要他们的类加载器也是同一个才可以认为他们是同一个
        // tomcat打破双亲委派机制就是基于这个方式
    }



}
