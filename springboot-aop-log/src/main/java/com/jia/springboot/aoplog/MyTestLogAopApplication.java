package com.jia.springboot.aoplog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@SpringBootApplication
public class MyTestLogAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestLogAopApplication.class, args);
    }
}
