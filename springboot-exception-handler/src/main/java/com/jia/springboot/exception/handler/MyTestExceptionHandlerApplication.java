package com.jia.springboot.exception.handler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@SpringBootApplication
public class MyTestExceptionHandlerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestExceptionHandlerApplication.class, args);
    }
}
