package com.jia.springboot.mybatisfirst;

import com.jia.springboot.mybatisfirst.entity.User;
import com.jia.springboot.mybatisfirst.mapper.UserMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@MapperScan(basePackages = {"com.jia.springboot.mybatisfirst.mapper"})
@SpringBootApplication
@RestController
public class MyTestMybatisApplication {

    @Autowired
    private UserMapper userMapper;

    public static void main(String[] args) {
        SpringApplication.run(MyTestMybatisApplication.class, args);
    }

    @GetMapping("/list")
    public List<User> list() {
        return userMapper.selectAllUser();
    }
}
