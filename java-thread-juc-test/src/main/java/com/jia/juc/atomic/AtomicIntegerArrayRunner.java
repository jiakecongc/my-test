package com.jia.juc.atomic;

import java.util.concurrent.atomic.AtomicIntegerArray;


public class AtomicIntegerArrayRunner {

    static int[] value = new int[]{1,2};
    static AtomicIntegerArray aiArray = new AtomicIntegerArray(value);

    public static void main(String[] args) {
        //todo 原子修改数组下标0的数值
        // 可以看出这里是复制了一份数据出来，而不是在原来的数组上面修改的
        aiArray.getAndSet(0,3);
        System.out.println(aiArray.get(0));
        System.out.println(value[0]);
}

}
