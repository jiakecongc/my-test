package com.jia.thread.schedule;


import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ScheduleThreadPoolRunner {

    public static void main(String[] args) {
        // 一般默认传一个核心线程数就好了，最大线程数其实是无效的了，因为使用了延迟队列，是无解的
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);

//        // 定义了延迟5s才执行这个callable,执行一次
//        ScheduledFuture<Integer> future = scheduledThreadPoolExecutor.schedule(() -> {
//            System.out.println("我要延迟5s执行");
//            return 1;
//        }, 5000, TimeUnit.MILLISECONDS);
//        //提交任务的线程-接着干活
//        //xxxxx
//        //xxxx
//        //xxxx
//        try {
//            System.out.println(future.get());
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }


//        scheduledThreadPoolExecutor.schedule(() -> {
//            System.out.println("我要延迟3s执行");
//            return 1;
//        }, 10000, TimeUnit.MILLISECONDS);

//        scheduledThreadPoolExecutor.schedule(() -> {
//            System.out.println("我要延迟3s执行");
//            return 1;
//        }, 3000, TimeUnit.MILLISECONDS);

//        scheduledThreadPoolExecutor.schedule(() -> {
//            System.out.println("我要延迟3s执行");
//            return 1;
//        }, 1000, TimeUnit.MILLISECONDS);



        // ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        // 定义一个线程
        //1634653841396----send heart beat
        //1634653843397----send heart beat
        //1634653845410----send heart beat
        //1634653847414----send heart beat
        //1634653849427----send heart beat
        //可以看出就是2s执行一次
        // 放任务进延迟队列中，延迟1s执行一次，每隔2s执行一次
//        scheduledThreadPoolExecutor.scheduleWithFixedDelay(() -> {
//            System.out.println(System.currentTimeMillis() + "----send heart beat");
//        }, 1000, 2000, TimeUnit.MILLISECONDS);

        // ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        // 定义一个线程
        //1634654057640----send heart beat
        //1634654062810--------task over....
        //1634654064813----send heart beat
        //1634654069980--------task over....
        //1634654071985----send heart beat
        //1634654077136--------task over....
        //1634654079140----send heart beat
        //1634654084319--------task over....
        //
        // 一个任务，放进延迟队列中，1秒后执行，执行5s,暂停了2s后，才又开始执行
        //
        // 放任务进延迟队列中，延迟1s执行一次，每隔2s执行一次
        // 如果任务执行需要5s，那么执行时间是怎么样的？
//        scheduledThreadPoolExecutor.scheduleWithFixedDelay(() -> {
//            System.out.println(System.currentTimeMillis() + "----send heart beat");
//               long starttime = System.currentTimeMillis(), nowtime = starttime;
//               while ((nowtime - starttime) < 5000) {
//                nowtime = System.currentTimeMillis();
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            System.out.println(System.currentTimeMillis() + "--------task over....");
//        }, 1000, 2000, TimeUnit.MILLISECONDS);



//         ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
//         定义一个线程
//1634654215669----send heart beat
//1634654217677----send heart beat
//1634654219675----send heart beat
//1634654221665----send heart beat
//1634654223662----send heart beat
//1634654225665----send heart beat

//        可以看出就是2s执行一次,和scheduleWithFixedDelay可以理解为没差别
//         放任务进延迟队列中，延迟1s执行一次，每隔2s执行一次
//        scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
//            System.out.println(System.currentTimeMillis() + "----send heart beat");
//        }, 1000, 2000, TimeUnit.MILLISECONDS);

        // ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        // 定义一个线程
        //1634654304497----send heart beat
        //1634654309693--------task over....
        //1634654309693----send heart beat
        //1634654314867--------task over....
        //1634654314867----send heart beat
        //1634654320026--------task over....
        //1634654320026----send heart beat
        //1634654325208--------task over....
        //1634654325208----send heart beat
        //1634654330364--------task over....
        //1634654330364----send heart beat
        //
        // 一个任务，放进延迟队列中，1秒后执行，执行5s,不会暂停了2s后，而是立刻就执行了。可以理解为2s后就有任务又在排队了（实际上不是这样子的）
        //
        // 放任务进延迟队列中，延迟1s执行一次，每隔2s执行一次
        // 如果任务执行需要5s，那么执行时间是怎么样的？
//        scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
//            System.out.println(System.currentTimeMillis() + "----send heart beat");
//               long starttime = System.currentTimeMillis(), nowtime = starttime;
//               while ((nowtime - starttime) < 5000) {
//                nowtime = System.currentTimeMillis();
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            System.out.println(System.currentTimeMillis() + "--------task over....");
//        }, 1000, 2000, TimeUnit.MILLISECONDS);


        // ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        // 定义一个线程
        // 1634654634860----send heart beat
        // 打印一次，因为报错了。而线程池中针对报错的处理，是重新生成一个 worker，等待新的任务执行，而原先的任务是别抛弃了的
        // 因此，自己的代码中一定要处理这个异常（延迟类线程池注意要处理这个问题）  主要方法：ThreadPoolExecutor中的runWork()方法中的异常处理：processWorkerExit
        //
        // 放任务进延迟队列中，延迟1s执行一次，每隔2s执行一次
        // 如果报错了，会怎么样？？？
//        scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
//            System.out.println(System.currentTimeMillis() + "----send heart beat");
//            throw new RuntimeException("报错了");
//        }, 1000, 2000, TimeUnit.MILLISECONDS);


        //定时类
        // timer里面抛异常的话，相当于是线程直接挂了，后面的其他任务不会再执行了。
        /*Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                log.info("send heart beat");
                throw new RuntimeException("unexpected error , stop working");
            }
        },1000,2000);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                log.info("send heart beat");
                throw new RuntimeException("unexpected error , stop working");
            }
        },1000,2000);*/

        /*scheduledThreadPoolExecutor.scheduleAtFixedRate(()->{
            log.info("running...");
        },1000,2000,TimeUnit.MILLISECONDS);*/

    }
}
