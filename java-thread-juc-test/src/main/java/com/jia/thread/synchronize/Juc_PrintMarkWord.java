package com.jia.thread.synchronize;

import org.openjdk.jol.info.ClassLayout;


public class Juc_PrintMarkWord {

    /**
     *
     * 偏向锁： C:System.lazyssssHashcode
     * 轻量级锁： LockRecord中的MarkWord中记录着hashCode
     * 重量级锁： hashCode记录在Monitor里面
     *
     *
     *
     * @param args
     * @throws InterruptedException
     */

    public static void main(String[] args) throws InterruptedException {
        // 需要sleep一段时间，因为java对于偏向锁的启动是在启动几秒之后才激活。
        // 因为jvm启动的过程中会有大量的同步块，且这些同步块都有竞争，如果一启动就启动
        // 偏向锁，会出现很多没有必要的锁撤销
        Thread.sleep(5000);
        T t = new T();
        //未出现任何获取锁的时候
        /**
         * 首先出现的是一个匿名偏向锁  101
         * com.yg.edu.T object internals:
         *  OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
         *       0     4        (object header)                           05 00 00 00 (00000101 00000000 00000000 00000000) (5)
         *       4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
         *       8     4        (object header)                           43 c1 00 f8 (01000011 11000001 00000000 11111000) (-134168253)
         *      12     4    int T.i                                       0
         * Instance size: 16 bytes
         * Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
         */
        System.out.println(ClassLayout.parseInstance(t).toPrintable());
        synchronized (t){
            // 获取一次锁之后
            /**
             *
             * 在获取一次锁之后，升级成偏向锁 101
             * com.yg.edu.T object internals:
             *  OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
             *       0     4        (object header)                           05 98 49 ad (00000101 10011000 01001001 10101101) (-1387685883)
             *       4     4        (object header)                           85 01 00 00 (10000101 00000001 00000000 00000000) (389)
             *       8     4        (object header)                           43 c1 00 f8 (01000011 11000001 00000000 11111000) (-134168253)
             *      12     4    int T.i                                       0
             * Instance size: 16 bytes
             * Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
             */
            System.out.println(ClassLayout.parseInstance(t).toPrintable());
        }
        // 输出hashcode

        /**
         * -------------------------------
         * 1213415012
         * -------------------------------
         */
        System.out.println("-------------------------------");
        System.out.println(t.hashCode());
        System.out.println("-------------------------------");
        System.out.println();
        // 计算了hashcode之后，将导致锁的升级
        /**
         * 在调用了对象的hashCode方法之后，这里是一个无锁的状态 001，
         * 注意：这里不是锁降级了，反而是升级成了轻量级锁。因为偏向锁调用hashCode后，发现偏向锁没有地方保持hashCode的地方，因此锁会升级成轻量级锁。可以看下面一个打印
         * 当锁升级成轻量级锁后，会在当前线程的线程栈上开辟一个LockRecord的空间，并且把MarkWord复制过来，因此这里看到的001，其实是MarkWord的，
         * 这里的MarkWord又会有一个指针指向轻量级锁，而轻量级锁的指针，指向的就是这个LockRecord,这里的MarkWord有hashCode
         *
         * com.yg.edu.T object internals:
         *  OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
         *       0     4        (object header)                           01 64 3e 53 (00000001 01100100 00111110 01010011) (1396597761)
         *       4     4        (object header)                           48 00 00 00 (01001000 00000000 00000000 00000000) (72)
         *       8     4        (object header)                           43 c1 00 f8 (01000011 11000001 00000000 11111000) (-134168253)
         *      12     4    int T.i                                       0
         * Instance size: 16 bytes
         * Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
         */
        System.out.println(ClassLayout.parseInstance(t).toPrintable());
        synchronized (t){
            // 再次获取锁
            /**
             * 这里是同一个线程，照理来说是不会升级成轻量级锁的，但是这里打印的是轻量级锁就是因为偏向锁调用了hashCode方法，导致锁变成了轻量级锁 00
             * com.yg.edu.T object internals:
             *  OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
             *       0     4        (object header)                           10 f3 df 2e (00010000 11110011 11011111 00101110) (786428688)
             *       4     4        (object header)                           3f 00 00 00 (00111111 00000000 00000000 00000000) (63)
             *       8     4        (object header)                           43 c1 00 f8 (01000011 11000001 00000000 11111000) (-134168253)
             *      12     4    int T.i                                       0
             * Instance size: 16 bytes
             * Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
             */
            System.out.println(ClassLayout.parseInstance(t).toPrintable());
        }
        /**
         *
         * 为什么这里打印的是MarkWord里的东西？？
         *
         *
         * com.yg.edu.T object internals:
         *  OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
         *       0     4        (object header)                           01 64 3e 53 (00000001 01100100 00111110 01010011) (1396597761)
         *       4     4        (object header)                           48 00 00 00 (01001000 00000000 00000000 00000000) (72)
         *       8     4        (object header)                           43 c1 00 f8 (01000011 11000001 00000000 11111000) (-134168253)
         *      12     4    int T.i                                       0
         * Instance size: 16 bytes
         * Space losses: 0 bytes internal + 0 bytes external = 0 bytes total
         */
        System.out.println(ClassLayout.parseInstance(t).toPrintable());
    }
}

class T{
    int i = 0;
}