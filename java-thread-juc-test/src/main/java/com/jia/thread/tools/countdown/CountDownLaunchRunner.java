package com.jia.thread.tools.countdown;

import java.util.concurrent.CountDownLatch;


public class CountDownLaunchRunner {

    static int sub = 0;
    static Object object = new Object();

    public static void main(String[] args) throws InterruptedException {
        long now = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(2);

//        for(int i=0;i<10;i++){
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        countDownLatch.await();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    synchronized (object){
//                        for(int j=0;j<1000;j++){
//                            System.out.println("线程开始执行" + Thread.currentThread().getName());
//                            sub++;
//                        }
//                    }
//
//                }
//            });
//        }
//        Thread.sleep(3000);
//        countDownLatch.countDown();
//        System.out.println("所有线程都准备好了，开始执行");



        //等待线程池中的2个任务执行完毕，否则一直等待,zk分布式锁
        new Thread(new SeeDoctorTask(countDownLatch)).start();
        new Thread(new QueueTask(countDownLatch)).start();
        countDownLatch.await();
        System.out.println("over，回家 cost:"+(System.currentTimeMillis()-now));
    }

}
