package com.jia.thread.lock;

import java.util.concurrent.locks.LockSupport;


public class Juc01_Thread_LockSupport {

    public static void main(String[] args) {

        /**
         * t0,开始执行!
         * 准备park住当前线程：t0 -----------第一次park住线程，这时线程的中断状态是false
         * --------------1false
         * 准备唤醒线程!t0        -----------外部线程unpark线程t0,这时候中断状态还是false   这说明LockSupport#park#unpark是不会修改线程的中断状态的
         * --------------2false
         * false                ------------ 被外部unpark唤醒的线程，中断标识还是false，因此调用Thread.interrupted()（注意这个接口会返回线程的中断状态且清空中断的标识）
         * 当前线程t0已经被唤醒....
         * 准备park住当前线程：t0   ------------第二次park住线程，线程的中断状态是false
         * --------------1false
         * --------------2true    ------------被park住的线程，在外部调用t0.interrupt(); 中断方法，该方法会给线程打上中断标识为true. 这时，park住的线程被唤醒，且中断标识为true
         * true                   ---------- 此处调用Thread.interrupted()方法，那么中断标识被清空为false，且返回原状态true
         * 当前线程t0已经被唤醒....
         * 准备park住当前线程：t0  ------------第三次次park住线程，由于线程的中断状态是false，因此这里会被阻塞住（注意，如果线程的中断状态是true，那么这个LockSupport#park将不会生效）
         * --------------1false
         */
        Thread t0 = new Thread(new Runnable() {
            @Override
            public void run() {
                Thread current = Thread.currentThread();
                System.out.println(current.getName() + ",开始执行!");
                for(;;){//spin 自旋
                    System.out.println("准备park住当前线程：" + current.getName());
                    // 这里调用park()的话，那么线程进行中断后，那么下一次就不会再被阻塞了。
                    System.out.println("--------------1" + Thread.currentThread().isInterrupted());
                    LockSupport.park();
                    // 这里使用的是isInterrupted() 那么不会清空线程的中断标志
//                    System.out.println(Thread.currentThread().isInterrupted());
                    // 这里使用的interrupted,会清空线程的中断标志为false
                    System.out.println("--------------2" + Thread.currentThread().isInterrupted());
                    System.out.println(Thread.interrupted());
                    System.out.println("当前线程" + current.getName() + "已经被唤醒....");

                }
            }
        },"t0");

        t0.start();

        try {
            Thread.sleep(2000);
            System.out.println("准备唤醒线程!" + t0.getName());
            LockSupport.unpark(t0);
            Thread.sleep(2000);
            t0.interrupt();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
