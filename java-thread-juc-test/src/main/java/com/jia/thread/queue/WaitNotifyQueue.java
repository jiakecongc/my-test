package com.jia.thread.queue;

import java.util.LinkedList;

public class WaitNotifyQueue<T> {

    // 容器，用来装东西
    private final LinkedList<T> queue = new LinkedList<>();

    public synchronized void put(T resource) throws InterruptedException {
        while (queue.size() >= 1) {
            // 队列满了，不能再塞东西了，轮询等待消费者取出数据
            System.out.println("生产者：队列已满，无法插入...");
            this.wait();
        }
        System.out.println("生产者：插入" + resource + "!!!");
        queue.addFirst(resource);
        this.notify();
    }

    public synchronized void take() throws InterruptedException {
        while (queue.size() <= 0) {
            // 队列空了，不能再取东西，轮询等待生产者插入数据
            System.out.println("消费者：队列为空，无法取出...");
            this.wait();
        }
        System.out.println("消费者：取出消息!!!");
        queue.removeLast();
        this.notify();
    }


    public static void main(String[] args) {
        WaitNotifyQueue<Integer> tWaitNotifyQueue = new WaitNotifyQueue<>();
        // 生产者
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    try {
                        tWaitNotifyQueue.put(i);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        // 如果是两个生产者，那么notify方法唤醒的可能还是生产者，而不是消费者，因此最好用notifyAll方法
        // notify随机唤醒
        // notifyAll唤醒所有阻塞的线程，重写抢夺执行权
        //wait/notify版本的缺点是随机唤醒容易出现“己方唤醒己方，最终导致全部线程阻塞”的乌龙事件，
        //虽然wait/notifyAll能解决这个问题，但唤醒全部线程又不够精确，会造成无谓的线程竞争
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    try {
                        tWaitNotifyQueue.put(i);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        // 消费者
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 100; i++) {
                    try {
                        tWaitNotifyQueue.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

}
