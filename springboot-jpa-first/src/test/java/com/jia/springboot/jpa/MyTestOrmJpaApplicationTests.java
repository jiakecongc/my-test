package com.jia.springboot.jpa;

import com.jia.springboot.jpsfirst.MyTestOrmJpaApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTestOrmJpaApplication.class)
public class MyTestOrmJpaApplicationTests {

    @Test
    public void contextLoads() {
    }

}
