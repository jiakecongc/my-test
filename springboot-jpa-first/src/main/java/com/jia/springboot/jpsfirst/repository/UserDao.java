package com.jia.springboot.jpsfirst.repository;

import com.jia.springboot.jpsfirst.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * User Dao
 * </p>
 *
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {

}
