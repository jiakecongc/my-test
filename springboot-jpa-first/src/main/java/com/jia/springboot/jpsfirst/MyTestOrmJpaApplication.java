package com.jia.springboot.jpsfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类
 * </p>
 *
 */
@SpringBootApplication
public class MyTestOrmJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyTestOrmJpaApplication.class, args);
    }
}
